// Loops.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

bool keepPlaying = true;

struct location
{
	int xCoord = 5;
	int yCoord = 5;
};

struct player
{
	location playerLocation;

	player()
	{
		playerLocation.xCoord = 5;
		playerLocation.yCoord = 5;
	}
};

struct suitComponent
{
	bool found;

	location componentLocation;

	suitComponent()
	{
		found = false;

		componentLocation.xCoord = -1;
		componentLocation.yCoord = -1;
	}
};


char playerInput = ' ';


int _tmain(int argc, _TCHAR* argv[])
{

	cout << "Play Game? (y/n)" << endl;

	cin >> playerInput;

	if (playerInput == 'n') keepPlaying = false;

	while (keepPlaying == true)
	{
		player tonyStark;

		suitComponent capacitor;
		suitComponent fan;
		suitComponent focusCrystal;
		suitComponent powerSource;
		suitComponent metalSheet;

		capacitor.componentLocation.xCoord = 2;
		capacitor.componentLocation.yCoord = 2;

		fan.componentLocation.xCoord = 3;
		fan.componentLocation.yCoord = 7;

		focusCrystal.componentLocation.xCoord = 7;
		focusCrystal.componentLocation.yCoord = 3;

		powerSource.componentLocation.xCoord = 9;
		powerSource.componentLocation.yCoord = 9;

		metalSheet.componentLocation.xCoord = 5;
		metalSheet.componentLocation.yCoord = 9;

		int starkX = tonyStark.playerLocation.xCoord = 5;
		int starkY = tonyStark.playerLocation.yCoord = 5;

		int capacitorX = capacitor.componentLocation.xCoord;
		int capacitorY = capacitor.componentLocation.yCoord;

		int fanX = fan.componentLocation.xCoord;
		int fanY = fan.componentLocation.yCoord;

		int focusCrystalX = focusCrystal.componentLocation.xCoord;
		int focusCrystalY = focusCrystal.componentLocation.yCoord;

		int powerSourceX = powerSource.componentLocation.xCoord;
		int powerSourceY = powerSource.componentLocation.yCoord;

		int metalSheetX = metalSheet.componentLocation.xCoord;
		int metalSheetY = metalSheet.componentLocation.yCoord;

		bool capacitorFound = capacitor.found;
		bool fanFound = fan.found;
		bool focusCrystalFound = focusCrystal.found;
		bool powerSourceFound = focusCrystal.found;
		bool metalSheetFound = metalSheet.found;

		cout << "You're a genius captured by terrorists to exploit your wealth of intelligence." << endl
			 <<	"To escape you construct a super-weapon, which you have nearly completed."	<< endl
			 << "You need to find only 5 more pieces, but be careful not to leave your workshop" << endl
			 << "or your captors will kill you!" << endl;


		while ((starkX < 10) && (starkX > 0) && (starkY < 10) && (starkY > 0))
		{
			cout << "Your location is (" << starkX << "," << starkY << ")" << endl;

			if (!(starkX < 9) || !(starkX > 1) || !(starkY < 9) || !(starkY > 1))
			{
				cout << "Warning! You are at the edge of your workspace." << endl;
			}

			cout << "Where would you like to go? North (n), East (e), South (s) or West (w)?" << endl;

			cin >> playerInput;


			switch (playerInput)
			{
			case 'n':
				starkY++;
				break;

			case 'e':
				starkX++;
				break;

			case 's':
				starkY--;
				break;

			case 'w':
				starkX--;
				break;

			default:
				break;
			}

			if ((starkX == capacitorX) && (starkY == capacitorY))
			{
				cout << "Congratulations, you found the capacitor!" << endl;

				capacitorX = -1;
				capacitorY = -1;

				capacitorFound = true;
			}

			if ((starkX == fanX) && (starkY == fanY))
			{
				cout << "Congratulations, you found the fan!" << endl;

				fanX = -1;
				fanY = -1;

				fanFound = true;
			}

			if ((starkX == focusCrystalX) && (starkY == focusCrystalY))
			{
				cout << "Congratulations, you found the focus crystal!" << endl;

				focusCrystalX = -1;
				focusCrystalY = -1;

				focusCrystalFound = true;
			}

			if ((starkX == powerSourceX) && (starkY == powerSourceY))
			{
				cout << "Congratulations, you found the powerSource!" << endl;

				powerSourceX = -1;
				powerSourceY = -1;

				powerSourceFound = true;
			}

			if ((starkX == metalSheetX) && (starkY == metalSheetY))
			{
				cout << "Congratulations, you found the metal sheet!" << endl;

				metalSheetX = -1;
				metalSheetY = -1;

				metalSheetFound = true;
			}

		}

		playerInput = ' ';

		starkX = 5;

		starkY = 5;

		if (capacitorFound == true && fanFound == true && focusCrystalFound == true && powerSourceFound == true && metalSheetFound == true)
		{
			cout << "You completed your suit and escaped, congratulations!" << endl;
		}
		else
		{
			cout << "You left the area and were shot!" << endl;
		}
		
		cout << "Play again? (y/n)" << endl;

		while ((playerInput != 'y') && (playerInput != 'n'))
		{
			cin >> playerInput;

			if ((playerInput != 'y') && (playerInput != 'n'))
			{
				cout << "Invalid input, try again" << endl;
			}
		}

		if (playerInput == 'n') keepPlaying = false;

	}
	cout;

	return 0;
}

